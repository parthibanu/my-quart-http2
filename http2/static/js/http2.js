document.addEventListener('DOMContentLoaded', function () {
    var calculate = function (operator) {
        fetch('/cal', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                a: document.getElementById("a").value,
                b: document.getElementById("b").value,
                operator: operator
            })
        }).then(function (response) {
            return response.json()
        }).then(function (data) {
            document.getElementById('result').innerText = data
        }).catch(function (error) {
            console.log('error happened')
            console.error(error)
        })
    };


    addbtn = document.getElementById('add')
    subbtn = document.getElementById('subtract')
    mulbtn = document.getElementById('multiply')
    divbtn = document.getElementById('divide')

    addbtn.onclick = function (event) {
        calculate('+');
        return false;
    }


    subbtn.onclick = function (event) {
        calculate('-');
        return false;
    }


    mulbtn.onclick = function (event) {
        calculate('*');
        return false;
    }


    divbtn.onclick = function (event) {
        calculate('/');
        return false;
    }

});