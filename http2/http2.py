from quart import Quart,render_template , make_push_promise,url_for,abort,jsonify, request
import asyncio
app = Quart(__name__)

@app.route('/')
async def index():
    await make_push_promise(url_for('static',filename='css/http2.css'))
    await make_push_promise(url_for('static',filename='js/http2.js'))
    return await render_template('index.html')
    #return   "hello World"

@app.route('/cal',methods=['POST'])
async def calulate():
    data = await request.get_json()
    operator = data['operator']
    try:
        a = int(data['a'])
        b = int(data['b'])
    except ValueError:
        abort(400)
    if operator == '+':
        return jsonify(a + b)
    elif operator == '-':
        return jsonify(a - b)
    elif operator == '*' :
        return jsonify( a * b)
    elif operator == '/':
        return jsonify( a / b)
    else:
        abort(400)
    


@app.cli.command('run')
def run():
     loop = asyncio.get_event_loop()
     if loop is None:
         print("loop is None")
     #future = asyncio.ensure_future()
     #loop.run_until_complete(future)
     #app.run()
     app.run(port=5000, certfile='cert.pem', keyfile='key.pem')

